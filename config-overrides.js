module.exports = function override(config, env) {
    config.resolve.alias.fs = 'pdfkit/js/virtual-fs.js';

    const newRules = [
        { enforce: 'post', test: /fontkit[/\\]index.js$/, loader: "transform-loader?brfs" },
        { enforce: 'post', test: /unicode-properties[/\\]index.js$/, loader: "transform-loader?brfs" },
        { enforce: 'post', test: /linebreak[/\\]src[/\\]linebreaker.js/, loader: "transform-loader?brfs" },
    ];

    for (var i = 0; i < newRules.length; ++i) {
        var newRule = newRules[i];
        config.module.rules.push(newRule);
    }

    return config;
}
